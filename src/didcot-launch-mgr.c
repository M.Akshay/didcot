/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "org.apertis.Didcot.Launch.h"
#include <canterbury/canterbury-platform.h>
#include "didcot-internal.h"

DataShareStruct data_struct = { NULL };

static void mime_app_list_free (MimeAppList *mime_app_list);
static void _didcot_handle_uri_scheme_mimes (OpenURIRequestData *request_data);
static void _didcot_handle_guess_content_for_mime (OpenURIRequestData *request_data);

void
open_uri_request_data_free (OpenURIRequestData *data)
{
  g_return_if_fail (data != NULL);
  g_clear_object (&data->invocation);
  g_free (data->uri);
  g_clear_pointer (&data->app_mime_list, mime_app_list_free);
  g_free (data);
}

static void
didcot_handle_url (OpenURIRequestData *request_data)
{
  DIDCOT_DEBUG ("uri %s", request_data->uri);
  if (g_str_has_prefix (request_data->uri, "file://"))
    _didcot_handle_guess_content_for_mime (request_data);
  else
    _didcot_handle_uri_scheme_mimes (request_data);
}

static void
_didcot_build_app_launch_db (OpenURIRequestData *request_data)
{
  g_autoptr (GPtrArray) entry_points = NULL;
  guint i;
  CbyEntryPoint *entry_point = NULL;
  const gchar *id = NULL;
  const gchar *const *types = NULL;

  DIDCOT_DEBUG ("enter");

  if (data_struct.entry_point_index == NULL)
    {
      g_dbus_method_invocation_return_dbus_error (request_data->invocation,
                                                  "org.apertis.Didcot.Launch.Error",
                                                  "Couldn't get entry points information");
      open_uri_request_data_free (request_data);
      return;
    }

  DIDCOT_DEBUG ("Retrieving entry points from %p", data_struct.entry_point_index);
  entry_points = cby_entry_point_index_get_entry_points (data_struct.entry_point_index);
  for (i = 0; i < entry_points->len; i++)
    {
      entry_point = g_ptr_array_index (entry_points, i);
      id = cby_entry_point_get_id (entry_point);
      DIDCOT_DEBUG ("entry point %s", id);
      types = cby_entry_point_get_supported_types (entry_point);
      while (types && types[0])
        {
          gchar *orig_key = NULL;
          GList *locallist = NULL;
          GList *element = NULL;

          DIDCOT_DEBUG ("   %s", types[0]);

          if (g_hash_table_lookup_extended (data_struct.mime_hash, id,
                                            (gpointer) &orig_key,
                                            (gpointer) &locallist))
            {
              g_hash_table_steal (data_struct.mime_hash, id);
            }
          else
            {
              orig_key = g_strdup (id);
            }

          if (locallist != NULL)
            {
              element = g_list_find_custom (locallist, types[0],
                                            (GCompareFunc) g_strcmp0);
            }

          if (element == NULL)
            locallist = g_list_append (locallist, g_strdup (types[0]));

          g_hash_table_replace (data_struct.mime_hash, orig_key, locallist);

          types++;
        }
    }

  didcot_handle_url (request_data);
}

static void
_didcot_launch_list_free_func (gpointer data)
{
  g_list_free_full (data, g_free);
}

static void search_for_apps_for_given_type(gpointer app_name , gpointer value ,gpointer userdata )
{
  MimeAppList *mime_app_list = userdata;
  GList *supported_types = NULL;

  DIDCOT_DEBUG ("app_name %s", (char *) app_name);
  for (supported_types = value; supported_types != NULL; supported_types = supported_types->next)
    {
      DIDCOT_DEBUG ("comparing %s to %s", (char *) supported_types->data, (char *) mime_app_list->type);
      if (g_strcmp0 (supported_types->data, mime_app_list->type) == 0)
        {

          mime_app_list->app_list = g_list_prepend (mime_app_list->app_list,
                                                    g_strdup (app_name));
        }
    }
}


static void
entry_point_open_uri_cb (GObject *source_object,
                         GAsyncResult *res,
                         gpointer user_data)
{
  GError *error = NULL;
  OpenURIRequestData *request_data = user_data;
  gboolean open_success = FALSE;

  open_success = cby_entry_point_open_uri_finish (CBY_ENTRY_POINT (source_object),
                                                  res,
                                                  &error);
  if (open_success)
    didcot_launch_complete_open_uri (NULL, request_data->invocation);
  else
    g_dbus_method_invocation_take_error (request_data->invocation, error);

  open_uri_request_data_free (request_data);
}

void
launch_app_with_url (const gchar *file_path,
                     const gchar *mimetype,
                     const gchar *app_name,
                     OpenURIRequestData *request_data)
{
  GVariantBuilder gvb;
  GVariant *platform_data = NULL;
  g_autoptr (CbyEntryPoint) entry_point = NULL;
  const gchar *content_types[] = { NULL, NULL };

  content_types[0] = mimetype;

  g_variant_builder_init (&gvb, G_VARIANT_TYPE_VARDICT);
  g_variant_builder_add (&gvb, "{sv}", "X-Apertis-ContentTypes",
                         g_variant_new ("^as", content_types));
  /* Floating ref, ownership taken by open_uri_async */
  platform_data = g_variant_builder_end (&gvb);

  entry_point = cby_entry_point_index_get_by_id (data_struct.entry_point_index,
                                                 app_name);
  cby_entry_point_open_uri_async (entry_point, file_path, platform_data, NULL,
                                  entry_point_open_uri_cb, request_data);
}

gchar *
didcot_get_icon_path (CbyEntryPoint *entry_point)
{
  gchar *ret = NULL;
  GIcon *icon = cby_entry_point_get_icon (entry_point);
  if (icon == NULL)
    return NULL;

  if (G_IS_THEMED_ICON (icon))
    {
      g_autofree gchar *tmp = g_icon_to_string (icon);

      /* FIXME: use GtkIconTheme instead of hard-coding one size */
      if (tmp != NULL)
        ret = g_strdup_printf ("/usr/share/icons/hicolor/36x36/apps/%s.png", tmp);
    }
  else if (G_IS_FILE_ICON (icon))
    {
      GFile *file = g_file_icon_get_file (G_FILE_ICON (icon));

      if (file)
        ret = g_file_get_path (file);
    }
  else
    {
      DIDCOT_DEBUG ("icon of type %s not handled yet", G_OBJECT_TYPE_NAME (icon));
    }

  return ret;
}

static void
launch_desired_app_with_url (OpenURIRequestData *request_data)
{
  MimeAppList *mime_app_list = NULL;

  g_return_if_fail (request_data != NULL);
  mime_app_list = request_data->app_mime_list;
  if (mime_app_list->app_list != NULL)
    {
      if (g_list_length (mime_app_list->app_list) == 1)
        {
          launch_app_with_url (mime_app_list->uri,
                               mime_app_list->type,
                               mime_app_list->app_list->data,request_data);
        }
      else
        {
          if (data_struct.popup_proxy != NULL)
            {
              didcot_show_selection_popup_for_request (request_data);
            }
          else
            {
              if (data_struct.request_list->len < 1024)
                {
                  g_ptr_array_add (data_struct.request_list, request_data);
                }
              else
                {
                  g_dbus_method_invocation_return_dbus_error (request_data->invocation,
                                                              "org.apertis.Didcot.Error",
                                                              "Too many requests to handle");
                  open_uri_request_data_free (request_data);
                }
            }
        }
    }
}

static void
mime_app_list_free (MimeAppList *mime_app_list)
{
  g_return_if_fail (mime_app_list != NULL);
  g_free (mime_app_list->type);
  g_free (mime_app_list->uri);
  g_list_free_full (mime_app_list->app_list, g_free);
  g_free (mime_app_list);
}

static void
_didcot_handle_guess_content_for_mime (OpenURIRequestData *request_data)
{
  gchar* file_mime = g_content_type_guess (request_data->uri, NULL, 0, NULL);

  DIDCOT_DEBUG ("%p %s", request_data, request_data->uri);

  if (g_content_type_is_unknown (file_mime))
    {

      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation,
          "org.apertis.Didcot.Launch.Error",
          "Unable to detect the mime type");
      g_free (file_mime);
      open_uri_request_data_free (request_data);
      return;
    }
  request_data->app_mime_list = g_new0 (MimeAppList, 1);
  request_data->app_mime_list->type = g_strdup (file_mime);
  request_data->app_mime_list->uri = g_strdup (request_data->uri);
  request_data->app_mime_list->app_list = NULL;
  g_free (file_mime);
  g_hash_table_foreach (data_struct.mime_hash, search_for_apps_for_given_type,
                        request_data->app_mime_list);
  if (request_data->app_mime_list->app_list == NULL)
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation, "org.apertis.Didcot.Error.NoApplicationFound",
          "No Application Found");
      open_uri_request_data_free (request_data);
      return;
    }
  launch_desired_app_with_url (request_data);
}

static void
_didcot_handle_uri_scheme_mimes (OpenURIRequestData *request_data)
{
  gchar *uri_scheme = g_uri_parse_scheme (request_data->uri);

  request_data->app_mime_list = g_new0 (MimeAppList, 1);
  request_data->app_mime_list->type = g_strconcat ("x-scheme-handler", "/",
                                                   uri_scheme, NULL);
  request_data->app_mime_list->app_list = NULL;
  request_data->app_mime_list->uri = g_strdup (request_data->uri);
  g_hash_table_foreach (data_struct.mime_hash, search_for_apps_for_given_type,
                        request_data->app_mime_list);
  g_free (uri_scheme);
  if (request_data->app_mime_list->app_list == NULL)
    {
      g_dbus_method_invocation_return_dbus_error (
          request_data->invocation, "org.apertis.Didcot.Error.NoApplicationFound",
          "No Application Found");
      open_uri_request_data_free (request_data);
      return;
    }
  launch_desired_app_with_url (request_data);
}

static gboolean
handle_open_uri_cb (DidcotLaunch *object, GDBusMethodInvocation *invocation,
                    const gchar *uri, gpointer user_data)
{
  OpenURIRequestData *request_data;

  request_data = g_new0 (OpenURIRequestData, 1);
  request_data->invocation = g_object_ref (invocation);
  request_data->uri = g_strdup (uri);
  request_data->app_mime_list = NULL;

  _didcot_build_app_launch_db (request_data);
  return TRUE;
}

void app_launch_interface_initialize(GDBusConnection *connection)
{
 DidcotLaunch *app_launch_manager = didcot_launch_skeleton_new();

  g_signal_connect (app_launch_manager, "handle-open-uri",
                    G_CALLBACK (handle_open_uri_cb), NULL);

	  if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (app_launch_manager),
	                                         connection,
	                                         "/org/apertis/Didcot/Launch",
	                                          NULL))
	  {
	    DIDCOT_WARNING("skeleton export error");
	  }


}

void
app_launch_init (void)
{
  GError *error = NULL;
  g_autoptr (CbyComponentIndex) component_index = NULL;

  /* Initialise all resources for open uri */
  data_struct.mime_hash = g_hash_table_new_full (g_str_hash, g_str_equal,
                                                 g_free,
                                                 _didcot_launch_list_free_func);

  component_index = cby_component_index_new (CBY_COMPONENT_INDEX_FLAGS_NONE, &error);
  if (error)
    {
      DIDCOT_WARNING ("%s: code %d: %s", g_quark_to_string (error->domain),
                      error->code, error->message);
      g_error_free (error);
      return;
    }

  data_struct.entry_point_index = cby_entry_point_index_new (component_index);
  DIDCOT_DEBUG ("Created entry points index at %p", data_struct.entry_point_index);
}
