/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#include "didcot-internal.h"
#include "barkway.h"

static void
selection_popup_shown_cb (GObject *source_object, GAsyncResult *res,
                          gpointer user_data)
{
  g_autoptr (GError) error = NULL;
  OpenURIRequestData *request_data = user_data;

  barkway_service_call_selection_popup_finish (data_struct.popup_proxy, res,
                                               &error);

  if (error != NULL)
    {
      g_signal_handler_disconnect (source_object,
                                   request_data->confirmation_result_handler_id);
      g_signal_handler_disconnect (source_object, request_data->error_handler_id);
      g_signal_handler_disconnect (source_object, request_data->popup_status_handler_id);
      g_dbus_method_invocation_return_gerror (request_data->invocation, error);
      open_uri_request_data_free (request_data);
    }
}

static void
handle_confirmation_result_for_open_uri_cb (BarkwayService *object,
                                            const gchar *app_name,
                                            const gchar *title,
                                            const gchar *confirmation_result,
                                            gint confirmation_value,
                                            gpointer user_data)
{
  if (g_strcmp0 (DATA_EXCHANGE_MGR_NAME, app_name) == 0)
    {
      if (!g_strcmp0 (title, "launch-app-with-uri-scheme"))
        {
          OpenURIRequestData *request_data = user_data;

          g_signal_handler_disconnect (object,
                                       request_data->confirmation_result_handler_id);
          g_signal_handler_disconnect (object, request_data->error_handler_id);
          g_signal_handler_disconnect (object, request_data->popup_status_handler_id);

          launch_app_with_url (request_data->app_mime_list->uri,
                               request_data->app_mime_list->type,
                               confirmation_result, request_data);
        }
    }
}

static void
handle_error_msg_for_open_uri_cb (BarkwayService *object,
                                  const gchar *arg_app_name,
                                  const gchar *arg_error_msg,
                                  gpointer user_data)
{
  if (g_strcmp0 (DATA_EXCHANGE_MGR_NAME, arg_app_name) == 0)
    {
      OpenURIRequestData *request_data = user_data;

      g_signal_handler_disconnect (object,
                                   request_data->confirmation_result_handler_id);
      g_signal_handler_disconnect (object, request_data->error_handler_id);
      g_signal_handler_disconnect (object, request_data->popup_status_handler_id);
      if (request_data->invocation)
        {
          g_dbus_method_invocation_return_dbus_error (
              request_data->invocation, "org.apertis.Didcot.Error",
              arg_error_msg);
        }
      open_uri_request_data_free (request_data);
    }
}

static void
handle_popup_status_cb (BarkwayService *object,
                        const gchar *app_name,
                        const gchar *title,
                        const gchar *popup_status,
                        gpointer user_data)
{
  if (!g_strcmp0 (DATA_EXCHANGE_MGR_NAME, app_name) &&
      !g_strcmp0 (title, "launch-app-with-uri-scheme") &&
      (!g_strcmp0 (popup_status, "CANCELLED") ||
       !g_strcmp0 (popup_status, "HIDDEN")))
    {
      OpenURIRequestData *request_data = user_data;

      g_signal_handler_disconnect (object, request_data->popup_status_handler_id);
      g_signal_handler_disconnect (object, request_data->confirmation_result_handler_id);
      g_signal_handler_disconnect (object, request_data->error_handler_id);
      g_dbus_method_invocation_return_dbus_error (request_data->invocation,
                                                  "org.apertis.Didcot.Error",
                                                  "Application launch cancelled");
      open_uri_request_data_free (request_data);
    }
}

void
didcot_show_selection_popup_for_request (OpenURIRequestData *request_data)
{
  GList *iter = NULL;
  GVariantBuilder gvb;
  GVariant *display_text = NULL;
  GVariant *image_info = NULL;
  GVariant *item_info = NULL;
  GVariant *rows_info = NULL;
  MimeAppList *mime_app_list = NULL;
  GVariantBuilder row_info_builder;

  g_return_if_fail (request_data != NULL);
  mime_app_list = request_data->app_mime_list;
  g_variant_builder_init (&gvb, G_VARIANT_TYPE ("a{ss}"));
  g_variant_builder_add (&gvb, "{ss}", "Text-Active", "open with");
  display_text = g_variant_builder_end (&gvb);
  g_variant_builder_init (&row_info_builder, G_VARIANT_TYPE_ARRAY);
  g_variant_builder_init (&gvb, G_VARIANT_TYPE ("a{ss}"));
  g_variant_builder_add (&gvb, "{ss}", "AppIcon", "NULL");
  g_variant_builder_add (&gvb, "{ss}", "MsgIcon", "NULL");
  image_info = g_variant_builder_end (&gvb);
  iter = mime_app_list->app_list;
  while (iter != NULL)
    {
      gchar *title = iter->data;
      g_autoptr (CbyEntryPoint) entry_point = NULL;
      const gchar *display_name = NULL;
      g_autofree gchar *icon_path = NULL;

      entry_point = cby_entry_point_index_get_by_id (data_struct.entry_point_index,
                                                     title);
      display_name = cby_entry_point_get_display_name (entry_point);
      icon_path = didcot_get_icon_path (entry_point);

      g_variant_builder_init (&gvb, G_VARIANT_TYPE ("a{ss}"));
      g_variant_builder_add (&gvb, "{ss}", "RowId", title);
      g_variant_builder_add (&gvb, "{ss}", "Image", icon_path);
      g_variant_builder_add (&gvb, "{ss}", "Text", display_name);
      item_info = g_variant_builder_end (&gvb);
      g_variant_builder_add_value (&row_info_builder, item_info);
      iter = iter->next;
    }
  rows_info = g_variant_builder_end (&row_info_builder);

  request_data->confirmation_result_handler_id = g_signal_connect (
      data_struct.popup_proxy, "confirmation-result",
      G_CALLBACK (handle_confirmation_result_for_open_uri_cb),
      request_data);

  request_data->error_handler_id = g_signal_connect (
      data_struct.popup_proxy, "error-msg",
      G_CALLBACK (handle_error_msg_for_open_uri_cb), request_data);

  request_data->popup_status_handler_id = g_signal_connect (data_struct.popup_proxy,
                                                            "popup-status",
                                                            G_CALLBACK (handle_popup_status_cb),
                                                            request_data);

  barkway_service_call_selection_popup (data_struct.popup_proxy,
                                        DATA_EXCHANGE_MGR_NAME,
                                        "launch-app-with-uri-scheme",
                                        display_text, rows_info,
                                        image_info, NULL,
                                        selection_popup_shown_cb,
                                        request_data);
}

static void
handle_open_uri_request_list (void)
{
  while (data_struct.request_list->len > 0)
    {
      OpenURIRequestData *request_data = NULL;

      request_data = g_ptr_array_remove_index (data_struct.request_list, 0);
      didcot_show_selection_popup_for_request (request_data);
    }
}

static void
handle_open_uri_request_list_for_error (GError *error)
{
  g_return_if_fail (error != NULL);

  while (data_struct.request_list->len > 0)
    {
      OpenURIRequestData *request_data = NULL;

      request_data = g_ptr_array_remove_index (data_struct.request_list, 0);
      g_dbus_method_invocation_return_gerror (request_data->invocation, error);
      open_uri_request_data_free (request_data);
    }
}

static void popup_service_proxy_clb( GObject *source_object, GAsyncResult *res,gpointer user_data)
{
 GError *error=NULL;
 DIDCOT_DEBUG("enter");
 data_struct.popup_proxy=barkway_service_proxy_new_finish (res,&error);

  if (error)
    {
      DIDCOT_WARNING ("%s: code %d: %s", g_quark_to_string (error->domain),
                     error->code, error->message);
      handle_open_uri_request_list_for_error (error);
      g_error_free (error);
      return;
    }
  handle_open_uri_request_list ();
}

static void name_appeared (GDBusConnection *connection,
               	   	   	   const gchar     *name,
               	   	   	   const gchar     *name_owner,
               	   	   	   gpointer         user_data)
{
  DIDCOT_DEBUG("enter");

  barkway_service_proxy_new( connection,
                                  G_DBUS_PROXY_FLAGS_NONE,
                                  "org.apertis.Barkway",
                                  "/org/apertis/Barkway/Service",
                                  NULL,
                                  popup_service_proxy_clb,
                                  NULL);

}

static void
name_vanished(GDBusConnection *connection,
               const gchar     *name,
               gpointer         user_data)
{
  g_autoptr (GError) error = NULL;

  DIDCOT_DEBUG("enter");
  error = g_dbus_error_new_for_dbus_error ("org.apertis.Didcot.Error",
                                           "popup service is not available");
  handle_open_uri_request_list_for_error (error);
  g_clear_object (&data_struct.popup_proxy);
}

void
popup_client_handler_init (void)
{
  /* Starts watching name on the bus specified by bus_type */
  /* calls name_appeared_handler and name_vanished_handler when the name is known to have a owner or known to lose its owner */
  g_bus_watch_name (G_BUS_TYPE_SESSION,
                    "org.apertis.Barkway",
                    G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                    name_appeared,
                    name_vanished,
                    NULL,
                    NULL);
}
