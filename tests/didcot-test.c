/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <gio/gio.h>
#include "didcot.h"


static void
test_open_uri (gconstpointer user_data)

{
  GError *error = NULL;
  DidcotLaunch *launch_proxy = didcot_launch_proxy_new_for_bus_sync (
      G_BUS_TYPE_SESSION, G_BUS_NAME_WATCHER_FLAGS_NONE, "org.apertis.Didcot",
      "/org/apertis/Didcot/Launch", NULL, &error);
  g_assert_no_error (error);
  didcot_launch_call_open_uri_sync (launch_proxy, user_data, NULL,
                                    &error);
  g_assert_no_error (error);
}

gint main(gint argc, gchar *argv[])
{
  g_test_init (&argc, &argv, NULL);
  g_test_set_nonfatal_assertions ();

  g_test_add_data_func_full ("/didcot-tests/test_open_uri", (gpointer)"didcot-test://foobar", test_open_uri, NULL);

  return g_test_run ();
}
