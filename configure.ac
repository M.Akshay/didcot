m4_define([didcot_major], [0])
m4_define([didcot_minor], [2020])
m4_define([didcot_micro], [1])
m4_define([didcot_nano], [0])

m4_define([didcot_base_version],
          [didcot_major.didcot_minor.didcot_micro])

m4_define([didcot_version],
          [m4_if(didcot_nano, 0,
                [didcot_base_version],
                [didcot_base_version].[didcot_nano])])

# Before making a release, the version info should be modified.
# Follow these instructions sequentially:
#   1. If the library source code has changed at all since the last update, then
#      increment revision (‘c:r:a’ becomes ‘c:r+1:a’).
#   2. If any interfaces have been added, removed, or changed since the last
#      update, increment current, and set revision to 0.
#   3. If any interfaces have been added since the last public release, then
#      increment age.
#   4. If any interfaces have been removed or changed since the last public
#      release, then set age to 0. Also increment didcot_major.
m4_define([didcot_lt_current], [1])
m4_define([didcot_lt_revision], [4])
m4_define([didcot_lt_age], [0])

m4_define([didcot_lt_version],
[didcot_lt_current:didcot_lt_revision:didcot_lt_age])

AC_PREREQ([2.62])
AC_INIT([didcot], didcot_version)
AC_CONFIG_AUX_DIR(config)
AC_CONFIG_MACRO_DIR([m4])
AM_MAINTAINER_MODE([enable])

AC_SUBST(DIDCOT_VERSION,didcot_version)
AC_SUBST(DIDCOT_LT_VERSION,didcot_lt_version)
AC_SUBST(DIDCOT_API_VERSION,didcot_major)

AM_INIT_AUTOMAKE([subdir-objects -Wno-portability tar-ustar])
AM_SILENT_RULES([yes])

AC_PROG_CC
AC_PROG_CXX
AC_ISC_POSIX
AC_STDC_HEADERS
AC_SEARCH_LIBS([strerror],[cposix])
AC_CHECK_FUNCS([memset])
AC_CHECK_HEADERS([fcntl.h stdlib.h string.h])

LT_INIT

AC_ARG_VAR(GDBUS_CODEGEN, [the gdbus-codegen programme ])
AC_PATH_PROG(GDBUS_CODEGEN, gdbus-codegen)
if test -z "$GDBUS_CODEGEN" ; then
  AC_MSG_ERROR([gdbus-codegen not found])
fi


#Check if Gobject-introspection enabled
GOBJECT_INTROSPECTION_CHECK([1.31.1])

dnl check for hotdoc
HOTDOC_CHECK([0.8], [c, dbus])

DIDCOT_PACKAGES_PUBLIC_GLIB="glib-2.0 gio-2.0"
DIDCOT_PACKAGES_PRIVATE_GLIB="gthread-2.0 gmodule-2.0 gio-unix-2.0"

DIDCOT_PACKAGES_PUBLIC_BARKWAY=""
DIDCOT_PACKAGES_PRIVATE_BARKWAY="barkway >= 0.3.0"

DIDCOT_PACKAGES_PUBLIC="$DIDCOT_PACKAGES_PUBLIC_GLIB $DIDCOT_PACKAGES_PUBLIC_BARKWAY"
DIDCOT_PACKAGES_PRIVATE="$DIDCOT_PACKAGES_PRIVATE_GLIB $DIDCOT_PACKAGES_PRIVATE_BARKWAY"

AC_SUBST([DIDCOT_PACKAGES_PUBLIC])
AC_SUBST([DIDCOT_PACKAGES_PUBLIC_GLIB])
AC_SUBST([DIDCOT_PACKAGES_PRIVATE])

PKG_CHECK_MODULES([GLIB], [$DIDCOT_PACKAGES_PUBLIC_GLIB $DIDCOT_PACKAGES_PRIVATE_GLIB])
PKG_CHECK_MODULES([BARKWAY], [$DIDCOT_PACKAGES_PUBLIC_BARKWAY $DIDCOT_PACKAGES_PRIVATE_BARKWAY])

PKG_CHECK_MODULES([CANTERBURY_PLATFORM], [canterbury-platform-0 >= 0.1703.5 ])


DIDCOT_FI=' -ldidcotlaunchiface -ldidcotshareiface'
AC_SUBST(DIDCOT_FI)

AC_ARG_WITH([systemduserunitdir],
  [AC_HELP_STRING([--with-systemduserunitdir=DIR],
    [path to systemd service directory])],
  [systemduserunitdir="${withval}"],
  [systemduserunitdir='${prefix}/lib/systemd/user'])
AC_SUBST([systemduserunitdir])

AX_COMPILER_FLAGS([], [],                                                                                                                                
                  dnl TODO: fix the warnings and change this from [yes] to                                                                               
                  dnl [m4_if(didcot_nano, 0, [yes], [no])],                                                                                             
                  [yes])

AC_REQUIRE_AUX_FILE([tap-driver.sh])
# installed-tests
AC_ARG_ENABLE([always_build_tests],
              [AS_HELP_STRING([--enable-always-build-tests],
                             [Enable always building tests (default: yes)])],
                             [],
              [enable_always_build_tests=yes])
AC_ARG_ENABLE([installed_tests],
              [AS_HELP_STRING([--enable-installed-tests],
                             [Install test programs (default: no)])],
                             [],
              [enable_installed_tests=no])

AM_CONDITIONAL([ENABLE_ALWAYS_BUILD_TESTS],
               [test "$enable_always_build_tests" = "yes"])
AC_SUBST([ENABLE_ALWAYS_BUILD_TESTS],[$enable_always_build_tests])

AM_CONDITIONAL([ENABLE_INSTALLED_TESTS],
               [test "$enable_installed_tests" = "yes"])
AC_SUBST([ENABLE_INSTALLED_TESTS],[$enable_installed_tests])

# code coverage
AX_CODE_COVERAGE



AC_CONFIG_FILES([
Makefile
docs/Makefile
docs/reference/Makefile
didcot-]didcot_major[.pc
src/Makefile
tests/Makefile
client/Makefile
])

AC_OUTPUT


dnl Summary
echo "                      didcot "
echo "                      --------------"
echo "
        documentation                  : ${enable_documentation}
        code coverage                  : ${enable_code_coverage}
        compiler warings               : ${enable_compile_warnings}
        Test suite                     : ${enable_modular_tests} "
